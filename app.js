var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cookieSession = require('cookie-session')
var mongodb = require('mongodb');
var mongoose = require('mongoose');
var findOrCreate = require('mongoose-find-or-create');
var database = require('./database');
var helmet = require('helmet')
var User = require('./routes/models/user');

/* OAUTH */
var passport = require('passport');
var config = require('./oauth.js');
var GoogleStrategy = require('passport-google-oauth2').Strategy;
var PassportUtils = require('./routes/utils/passport_utils')

var mongoPassword = "^mQ!%{YC8t$]bYM7";

/*mongoose.connect("mongodb://" + database.mongo.user + ":" +
encodeURIComponent(mongoPassword) + "@" +
    database.mongo.hostString);*/

mongoose.connect('mongodb://localhost:27017/swlfg');
// When successfully connected
mongoose.connection.on('connected', function () {
  console.log('Mongoose default connection open');
});

// If the connection throws an error
mongoose.connection.on('error',function (err) {
  console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
  console.log('Mongoose default connection disconnected');
});

PassportUtils.configure(function(result) {});

var index = require('./routes/index');
var register = require('./routes/register');
var r_success = require('./routes/register_success');
var account = require('./routes/users/account');
var find = require('./routes/users/find');
var validate_token = require('./routes/utils/validate_token');

// Posts API
var create_post = require('./routes/api/create_post');

var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieSession({
  name: 'xdrawrdarkdank',
  keys: ["meme1", "meme2"],
  /*cookie: {
    secure: true,
    httpOnly: true,
    domain: 'example.com',
    path: 'foo/bar',
    expires: expiryDate
  },*/
  // Cookie Options
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
}))
app.set('superSecret', database.secret); // secret variable
app.use(helmet())
app.use(passport.initialize());
app.use(passport.session());

app.use('/', index);
app.use('/register/step1', ensureNotAuthenticated, register);
app.use('/register/success', r_success);
app.use('/account', ensureAuthenticated, account);
app.use('/find', ensureAuthenticated, find);

app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

// API
app.all('/api/*', validate_token);
app.use('/api/create_post', create_post);

// Globals
app.locals.clients = ["Origin", "Steam", "PSN", "Xbox"]
app.locals.activities = ["GA", "HVV", "BS", "SA"]
app.locals.activities_full_name = {
  "GA": "Galactic Assault",
  "HVV": "Heroes vs Villains",
  "BS": "Blast / Strike",
  "SA": "Starfighter Assault"
}
app.locals.clients_urls = {
  "Origin": "origin.com",
  "Steam": "steampowered.com",
  "PSN": "playstation.com",
  "Xbox": "xbox.com"
}
app.locals.language = ["English", "French", "Spanish", "German", "Chinese", "Other"]
app.locals.experience = ["Casual", "Competitive"]

app.get('/auth/google', passport.authenticate('google', { scope: [
    'https://www.googleapis.com/auth/plus.profile.emails.read',
    'profile'
  ] }), function(req, res){});

app.get('/auth/google/callback', passport.authenticate('google'), function(req, res) {
  if(!res.headersSent) {
    res.redirect('/register/success');
  }
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


//pass user data
app.use(function(req, res, next) {
  res.locals.current_path = req.variable;
  next();
});

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/');
}

function ensureNotAuthenticated(req, res, next) {
  if (!req.isAuthenticated()) { return next(); }
  res.redirect('/');
}

var port = process.env.port || 4000;

var server = app.listen(port)

var io = require('socket.io')(server);
var socket = require('./routes/utils/socket')

var EventEmitter = require('events').EventEmitter;
var global_emitter = new EventEmitter()

io.on('connection', function(socket) {
  socket.on('joinChat', function() {
    socket.emit("joined")
    socket.join('partner_room')
  })
})

global_emitter.on("new_lfg", function(data) {
  io.to("partner_room").emit("new_lfg", data)
})

app.locals.global_emitter = global_emitter;
