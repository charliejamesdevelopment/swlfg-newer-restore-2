var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var lfg = new Schema({
  author: { type: Number, required: true},
  platform: { type: String, required: true},
  experience: { type: String, required: true},
  activity: { type: String, required: true},
  language: { type: String, required: true},
  notes: { type: String, default: 'No notes left...'},
  created_at: Date
});

lfg.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();
  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});

lfg.set('collection', 'lfg');

var Lfg = mongoose.model('Lfg', lfg);

// make this available to our users in our Node applications
module.exports = Lfg;
