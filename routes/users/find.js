var express = require('express');
var post_utils = require('../utils/post_utils');
var user = require('../models/user');
var router = express.Router();
var moment = require('moment')
var each = require('sync-each');

/* GET home page. */
router.get('/partner', function(req, res, next) {
  var utils = require('../utils/user_utils')(req);

  post_utils.getPosts(function(err, docs) {

    if(err) { console.log(err) }

    const new_ndocs = []

    var promise = new Promise(function(resolve, reject) {
      if(docs.length > 0) {
        each(docs,
          function (d,next) {
            user.findOne({oauthID: d.author}, function(err, user) {

              var author = "#UnknownUser";
              var platform = d.platform;

              if(!err && user) {

                utils.getLevelOfPlatform(d.author, platform, function(level) {

                  utils.createNDOC(d, user, level, function(ndoc) {
                    new_ndocs.push(ndoc)
                    next()
                  });

                })

              } else {
                reject("User can't be found.")
              }

            })
          },
          function () {
            resolve(new_ndocs)
          }
        )
      } else {
        resolve(null)
      }
    })

    promise.then(function(new_ndocs) {

      res.render('user/find_gamers', {

        documents: (docs ? new_ndocs : null),
        user: req.session.passport.user,
        token: req.session.token.token,
        title: "Find Gamers"

      });

    }, function(error) {

      if(error) {
        console.log(error)
      }

    })

  });

});


module.exports = router;
