var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  var utils = require('../utils/user_utils')(req);
  oauthID = req.session.passport.user.id;
  utils.getUser(oauthID, function(err, user_profile){
    if(err && !user_profile) {
      console.log(err)
    } else if(!err && user_profile) {
      res.render('user/account', {
        user: req.session.passport.user,
        user_info: user_profile,
        title: "Account"
      });
    } else if(!err && !user_profile) {
      console.log('idek what went wrong')
    }
  });
});

router.post('/update_gamertags', function(req, res, next) {
  var utils = require('../utils/user_utils')(req);
  oauthID = req.session.passport.user.id;

  var psn = req.body.psn == "" ? "N/A" : req.body.psn;
  var xbox = req.body.xbox == "" ? "N/A" : req.body.xbox;
  var origin = req.body.origin == "" ? "N/A" : req.body.origin;

  var gamertags = {
    psn: psn,
    xbox: xbox,
    origin: origin
  }

  utils.updateGamertags(oauthID, gamertags, function(err) {
    if(!err) {
      res.redirect('/account')
    } else {
      res.redirect('/')
    }
  })
});

router.post('/update_info', function(req, res, next) {
  var utils = require('../utils/user_utils')(req);
  oauthID = req.session.passport.user.id;

  var psn = req.body.psn == "" ? "1" : req.body.psn;
  var xbox = req.body.xbox == "" ? "1" : req.body.xbox;
  var origin = req.body.origin == "" ? "1" : req.body.origin;
  var bio = req.body.bio;

  var levels = {
    psn: psn,
    xbox: xbox,
    origin: origin
  }

  utils.updateInfo(oauthID, levels, bio, function(err) {
    if(!err) {
      res.redirect('/account')
    } else {
      res.redirect('/')
    }
  })
});

module.exports = router;
