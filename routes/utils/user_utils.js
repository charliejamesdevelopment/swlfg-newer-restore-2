var User = require('../models/user');
var moment = require('moment')
var Promise = require('promise')

module.exports = function(req){
  var validation_utils = require('../utils/validation_utils')(req);
  var clients = req.app.locals.clients
  var activities = req.app.locals.activities
  var activities_full_name = req.app.locals.activities_full_name
  var clients_urls = req.app.locals.clients_urls
  return {
    getUser: function(oauthID, callback) {
      User.findOne({oauthID: oauthID}, function(err, user) {
        if(err) {
          callback(err, null)
        } else {
          if(!err && user) {
            callback(err, user)
          } else {
            callback(null, null)
          }
        }
      });
    },
    getUserClientGamertag: function(user, client, fn) {
      var utils = require('./user_utils')(req)
      utils.getUser(user, function(err, user){
        if(!err && user) {
          var object_entries = Object.entries(user.gamertags);
          var loop = utils.loopObjectEntries(object_entries, client)

          loop.then(function(result) {
              fn(result)
          }, function(error) {
              if(error) {
                console.log(error)
              }
          });
        } else {
          fn(null)
        }
      })
    },
    getLevelOfPlatform: function(oauthID, platform, fn) {
      User.findOne({oauthID: oauthID}, function(err, user) {
        if(err) {
          fn(1)
        } else {
          if(!err && user) {
            if(validation_utils.checkInArray(clients, platform)) {
              platform = platform.toLowerCase()
              var level = 1
              var object_entries = Object.entries(user.levels);
              for(var i = 1; i < object_entries.length; i++) {
                kv = object_entries[i];
                if(kv[0].toLowerCase() == platform.toLowerCase()) {
                  level = kv[1]
                }
                if((i+1) == object_entries.length) {
                  fn(level)
                }
              }
            } else {
              fn(1)
            }
          } else {
            fn(1)
          }
        }
      });
    },
    loopObjectEntries: function(object_entries, activity) {
      return new Promise(function(resolve, reject) {
        for(var i = 0; i < object_entries.length; i++) {
          kv = object_entries[i];
          if(kv[0].toLowerCase() == activity.toLowerCase()) {
            resolve(kv[1])
          }
        }
        reject("Could not find relevant activity.")
      });
    },
    getClientUrl: function(client, fn) {
      var utils = require('./user_utils')(req)
      if(validation_utils.checkInArray(clients, client)) {
        var object_entries = Object.entries(clients_urls);

        var loop = utils.loopObjectEntries(object_entries, client)

        loop.then(function(result) {
            fn(result)
        }, function(error) {
            if(error) {
              console.log(error)
            }
        });
      } else {
        return null
      }
    },
    getActivity: function(activity, fn) {
      var utils = require('./user_utils')(req)

      if(validation_utils.checkInArray(activities, activity)) {
        var object_entries = Object.entries(activities_full_name);

        var loop = utils.loopObjectEntries(object_entries, activity)

        loop.then(function(result) {
            fn(result)
        }, function(error) {
            if(error) {
              console.log(error)
            }
        });
      } else {
        return null
      }
    },
    createNDOC: function(d, user, level, fn) {
      var utils = require('./user_utils')(req)
      utils.getActivity(d.activity, function(a) {
        utils.getClientUrl(d.platform, function(b) {
          utils.getUserClientGamertag(d.author, d.platform, function(c) {
            var author = user.name;
            var date = moment(d.created_at).fromNow()

            var n_doc = {
              author: c,
              platform: b,
              experience: d.experience,
              activity: a,
              created_at: date,
              language: d.language,
              notes: d.notes == "" ? "No notes have been left on this post." : d.notes,
              level: level
            }

            fn(n_doc)
          });
        })
      });
    },
    updateGamertags: function(oauthID, gamertags, fn) {
      var utils = require('./user_utils')(req)
      utils.getUser(oauthID, function(err, user) {
        if(!err && user) {
          user.gamertags = gamertags;
          user.save(function (err, updatedUser) {
            if (err) return handleError(err);
            console.log(updatedUser)
            fn(null)
          });
        } else {
          fn(err)
        }
      });
    },
    updateInfo: function(oauthID, levels, bio, fn) {
      var utils = require('./user_utils')(req)
      utils.getUser(oauthID, function(err, user) {
        if(!err && user) {
          user.levels = levels;
          user.bio = bio;
          user.save(function (err, updatedUser) {
            if (err) return handleError(err);
            console.log(updatedUser)
            fn(null)
          });
        } else {
          fn(err)
        }
      });
    },
    emitLfg: function(d, global_emitter) {
      var utils = require('./user_utils')(req)
      var promise = new Promise(function(resolve, reject) {
        utils.getUser(d.author, function(err, user) {
          var author = "#UnknownUser";
          var platform = d.platform;
          if(!err && user) {

            utils.getLevelOfPlatform(d.author, platform, function(level) {

              utils.createNDOC(d, user, level, function(ndoc) {

                resolve(ndoc)

              });

            })

          } else {
            reject("User can't be found.")
          }

        })
      })

      promise.then(function(result) {
        global_emitter.emit("new_lfg", result)
      }, function(err) {
        if(err) {
          console.log(err)
        }
      })
    }
  }
}
