var jwt = require('jwt-simple');

module.exports = function(req, res, next) {
  var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];

    if (token) {
      try {

        var decoded = jwt.decode(token, require('../../database').secret);
        if (decoded.exp <= Date.now()) {
          res.status(400).send({
            "status": 400,
            "message": "Token Expired"
          });
          return;
        }
        next()

      } catch (err) {
        res.status(500).send({
          "status": 500,
          "message": "Oops something went wrong",
          "error": err
        });
      }
    } else {
      res.status(401).send({
        "status": 401,
        "message": "Invalid Token or Key"
      });
      return;
    }
}
