var Lfg = require('../models/lfg');
var moment = require('moment')

module.exports = {
  getPosts : function (fn){
    Lfg.find({}).sort('created_at').exec(function(err, docs) {
      if(!err && docs) {
        fn(null, docs)
      } else if(err) {
        fn(err, null)
      } else {
        fn(null, null)
      }
    });
  }
}
